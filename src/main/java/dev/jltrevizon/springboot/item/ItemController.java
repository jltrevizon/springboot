package dev.jltrevizon.springboot.item;

import java.util.Optional;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import io.micrometer.core.annotation.Counted;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "/api/item", produces = "application/json")

public class ItemController {
    @Autowired
    private ItemRepository itemRepository;
    public static final Logger logger = LoggerFactory.getLogger(ItemController.class);

    public Supplier fetchItemCount() {
        return ()-> itemRepository.count();
    } 
    
    // constructor injector for exposing metrics at Actuator /prometheus 
    public ItemController(MeterRegistry registry) {
        Gauge.builder("itemcontroller.itemcount", fetchItemCount()).tag("version", "v1").description("itemcontroller descrip").register(registry);
    }

    @Timed(value = "item.get.time", description = "time to retrieve items", percentiles = { 0.5, 0.9 })
    @Operation(summary = "get list of items")
    @ApiResponse(responseCode = "200")
    @GetMapping
    public Iterable<Item> allItems() {
        logger.debug("doing allItems");
        return itemRepository.findAll();
    }
    @Counted()
    @Timed(value = "item.delete.time", description = "time to delete item", percentiles = { 0.5, 0.9 })
    @Operation(summary = "delete item by id")
    @ApiResponse(responseCode = "200")
    @DeleteMapping("/{id}") 
    public boolean deleteItem(@PathVariable() Long id) {
        logger.debug("called deleteItem");
        itemRepository.deleteById(id);
        return true;
    }
    @Timed(value = "item.find.time", description = "time to find a item", percentiles = { 0.5, 0.9 })
    @Operation(summary = "get item by id")
    @ApiResponse(responseCode = "200")
    @GetMapping("/{id}")
    public Optional<Item> getItem(@PathVariable() Long id) {
        logger.debug("called getItem");
        return itemRepository.findById(id);
    }

    @Timed(value = "item.update.time", description = "time to update a item", percentiles = { 0.5, 0.9 })
    @Operation(summary = "update item by id")
    @ApiResponse(responseCode = "200")
    @PutMapping()
    public Item updateItem(@RequestBody Item item) {
        logger.debug("called updateItem");
        return itemRepository.save(item);
    }
    @Timed(value = "item.create.time", description = "time to create a item", percentiles = { 0.5, 0.9 })
    @Operation(summary = "create item")
    @ApiResponse(responseCode = "200")
    @PostMapping
    public Item createItem(@RequestBody Item item) {
        logger.debug("called createItem");
        return itemRepository.save(item);
    }

}
