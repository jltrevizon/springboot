package dev.jltrevizon.springboot.item;

import dev.jltrevizon.springboot.PackagingType;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name= "item")
public class Item implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @Column(name = "require_fridge")
    private boolean requireFridge;
    private Long capacity;
    @Column(name = "packaging_type")
    @Enumerated(EnumType.STRING)
    private PackagingType packagingType;     

    public Item() {
    }
 
    public Item(Long id, String name, boolean requireFridge, Long capacity, PackagingType packagingType) {
        super();
        this.id = id;
        this.name = name;
        this.requireFridge = requireFridge;
        this.capacity = capacity;
        this.packagingType = packagingType;
    }

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getRequireFridge() {
        return requireFridge;
    }

    public void setRequireFridge(boolean requireFridge) {
        this.requireFridge = requireFridge;
    }

    public Long getCapacity() {
        return capacity;
    }

    public void setCapacity(Long capacity) {
        this.capacity = capacity;
    }

    public PackagingType getPackagingType() {
        return packagingType;
    }
     
    public void setPackagingType(PackagingType packagingType) {
        this.packagingType = packagingType;
    }

    @Override
    public String toString() {
        return "Item [id=" + id + 
            ", name=" + name + 
            ", requireFridge=" + requireFridge + 
            ", capacity=" + capacity + 
            ", packagingType=" + packagingType + 
        "]";
    }

}
