package dev.jltrevizon.springboot.item;

import dev.jltrevizon.springboot.item.ItemController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class ItemHealthIndicator implements HealthIndicator {

  @Autowired
  protected ItemController itemController;

  @Override
  public Health health() {
    int itemCount = ((Number)itemController.fetchItemCount().get()).intValue();
    if (itemCount>0)
      return new Health.Builder().up().withDetail("itemcount", itemCount).build();
    else
      return new Health.Builder().down().withDetail("itemcount", itemCount).build();
  }

}

